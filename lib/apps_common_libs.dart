// apps common libs

export 'package:flutter/material.dart';
export 'package:provider/provider.dart';
export 'package:sqflite/sqflite.dart';
export 'package:intl/date_symbol_data_local.dart';
export 'package:easy_splash_screen/easy_splash_screen.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:front_end_sie_pos/apps_scaffold.dart';
export 'package:currency_text_input_formatter/currency_text_input_formatter.dart';

export 'package:front_end_sie_pos/provider/apps_theme_provider.dart';
export 'package:front_end_sie_pos/provider/apps_user_provider.dart';
export 'package:front_end_sie_pos/provider/language_provider.dart';

export 'package:front_end_sie_pos/resource/apps_style_resource.dart';
export 'package:front_end_sie_pos/resource/apps_icon_resource.dart';
export 'package:front_end_sie_pos/resource/apps_image_resource.dart';
export 'package:front_end_sie_pos/resource/apps_public_model_resource.dart';
export 'package:front_end_sie_pos/resource/apps_public_variable_resource.dart';

export 'package:front_end_sie_pos/view/apps_splash_screen_page.dart';
export 'package:front_end_sie_pos/view/auth/apps_login_page.dart';
export 'package:front_end_sie_pos/view/auth/apps_register_page.dart';

export 'package:front_end_sie_pos/widget/login_page_widget/apps_clip_shadow_path_widget.dart';
export 'package:front_end_sie_pos/widget/login_page_widget/apps_curve_cliper_widget.dart';
export 'package:front_end_sie_pos/widget/login_page_widget/apps_main_login_widget.dart';
export 'package:front_end_sie_pos/widget/login_page_widget/apps_tail_login_widget.dart';
export 'package:front_end_sie_pos/widget/apps_appbar_widget.dart';
export 'package:front_end_sie_pos/widget/apps_icon_widget.dart';
export 'package:front_end_sie_pos/widget/apps_divider_widget.dart';
export 'package:front_end_sie_pos/widget/apps_bottom_navbar_widget.dart';
export 'package:front_end_sie_pos/widget/apps_button_widget.dart';
export 'package:front_end_sie_pos/widget/apps_textfield_widget.dart';


export 'package:front_end_sie_pos/function/apps_login_function.dart';

