
import 'package:front_end_sie_pos/apps_common_libs.dart';

void main() async {

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppsThemeProvider>(
            create: (_) => AppsThemeProvider()),
        ChangeNotifierProvider<AppsUserProvider>(
            create: (_) => AppsUserProvider())
      ],
      child: Consumer<AppsThemeProvider>(
        builder: (context, AppsThemeProvider provider, child) {
          return GestureDetector(
            onTap: () {
              FocusScopeNode currentfocus = FocusScope.of(context);
              if (!currentfocus.hasPrimaryFocus) {
                currentfocus.unfocus();
              }
            },
            child: MaterialApp(
                theme: provider.darkmode
                    ? AppsStylesResource.themeData(true, context)
                    : AppsStylesResource.themeData(false, context),
                debugShowCheckedModeBanner: false,
                home: const AppsSplasscreen()),
          );
        },
      ),
    );
  }
}



class AppsWrapper extends StatefulWidget {
  const AppsWrapper({super.key});

  @override
  State<AppsWrapper> createState() => _AppsWrapperState();
}

class _AppsWrapperState extends State<AppsWrapper> {
  @override
  Widget build(BuildContext context) {
    var userProv = Provider.of<AppsUserProvider>(context).appsuserdataprovider;
    setState(() {
      AppsPublicVariableResource.idUser = userProv.idUser;
      AppsPublicVariableResource.tipeUser = userProv.tipeUser;
      AppsPublicVariableResource.tipeMesinKasir = userProv.tipeMesinKasir;
      AppsPublicVariableResource.email = userProv.email;
      AppsPublicVariableResource.username = userProv.username;
      AppsPublicVariableResource.password = userProv.password;
      AppsPublicVariableResource.token = userProv.token;
      AppsPublicVariableResource.companyName = userProv.companyName;
      AppsPublicVariableResource.companyAddress = userProv.companyAddress;
      AppsPublicVariableResource.companyPhone = userProv.companyPhone;
    });
    if (userProv.isLogin) {
      return const AppsSplasscreen();
    } else {
      return const AppsLoginPage();
    }
  }
}

