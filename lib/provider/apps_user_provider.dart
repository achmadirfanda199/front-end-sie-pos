// apps user provider

import 'package:front_end_sie_pos/apps_common_libs.dart';

class AppsUserDataProvider {
  int idUser;
  String tipeUser;
  String tipeMesinKasir;
  String email;
  String username;
  String password;
  String token;
  String companyName;
  String companyAddress;
  String companyPhone;
  bool isLogin;

  AppsUserDataProvider({
    required this.idUser,
    required this.tipeUser,
    required this.tipeMesinKasir,
    required this.email,
    required this.username,
    required this.password,
    required this.token,
    required this.companyName,
    required this.companyAddress,
    required this.companyPhone,
    this.isLogin = false,
  });
}

class AppsUserProvider extends ChangeNotifier {
  late AppsUserDataProvider appsuserdataprovider;
  AppsUserProvider() {
    appsuserdataprovider = AppsUserDataProvider(
      idUser: 0,
      tipeUser: "",
      tipeMesinKasir: "",
      email: "",
      username: "",
      password: "",
      token: "",
      companyName: "",
      companyAddress: "",
      companyPhone: "",
      isLogin: false,
    );
    loadUserData();
    notifyListeners();
  }

  AppsUserDataProvider get appsUserDataProvider => appsuserdataprovider;

  //get data form login page
  void userLogin(AppsUserDataProvider appsUserDataProvider) async {
    final SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setInt("idUser", appsUserDataProvider.idUser);
    sp.setString("tipeUser", appsUserDataProvider.tipeUser);
    sp.setString("tipeMesinKasir", appsUserDataProvider.tipeMesinKasir);
    sp.setString("email", appsUserDataProvider.email);
    sp.setString("username", appsUserDataProvider.username);
    sp.setString("password", appsUserDataProvider.password);
    sp.setString("token", appsUserDataProvider.token);
    sp.setString("companyName", appsUserDataProvider.companyName);
    sp.setString("companyAddress", appsUserDataProvider.companyPhone);
    sp.setBool("isLogin", true);

    appsuserdataprovider = appsUserDataProvider;
    notifyListeners();
  }

// load data if user come back to apps (or apps closed)
  void loadUserData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    appsuserdataprovider = AppsUserDataProvider(
      idUser: sp.getInt("idUser") ?? 0,
      tipeUser: sp.getString("tipeUser") ?? "",
      tipeMesinKasir: sp.getString("tipeMesinKasir") ?? "",
      email: sp.getString("email") ?? "",
      username: sp.getString("username") ?? "",
      password: sp.getString("password") ?? "",
      token: sp.getString("token") ?? "",
      companyName: sp.getString("companyName") ?? "",
      companyAddress: sp.getString("companyAddress") ?? "",
      companyPhone: sp.getString("companyPhone") ?? "",
      isLogin: sp.getBool("isLogin") ?? true,
    );
    notifyListeners();
  }

  //logout
  void logOut() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setInt("idUser", 0);
    sp.setString("tipeUser", "");
    sp.setString("tipeMesinkasir", "");
    sp.setString("email", "");
    sp.setString("username", "");
    sp.setString("passowrd", "");
    sp.setString("token", "");
    sp.setString("companyName", "");
    sp.setString("companyPhone", "");
    sp.setBool("isLogin", false);
    appsuserdataprovider;
    notifyListeners();
  }
}
