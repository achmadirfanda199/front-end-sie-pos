// apps public variable

import 'package:front_end_sie_pos/apps_common_libs.dart';

class AppsPublicVariableResource {
  //jarak
  static Widget jarakheight8 = const SizedBox(
    height: 8,
  );
  static Widget jarakheight16 = const SizedBox(
    height: 16,
  );
  static Widget jarakheight24 = const SizedBox(
    height: 24,
  );
  static Widget jarakheight32 = const SizedBox(
    height: 32,
  );

  //width
  static Widget jarakwidth8 = const SizedBox(
    width: 8,
  );
  static Widget jarakwidth16 = const SizedBox(
    width: 16,
  );
  static Widget jarakwidth24 = const SizedBox(
    width: 24,
  );
  static Widget jarakwidth32 = const SizedBox(
    width: 32,
  );

  // public key variable
  static GlobalKey<FormState> formkey = GlobalKey<FormState>();

  // user variable
  static int idUser = 0;
  static String tipeUser = "";
  static String tipeMesinKasir = "";
  static String email = "";
  static String username = "";
  static String password = "";
  static String token = "";
  static String companyName = "";
  static String companyAddress = "";
  static String companyPhone = "";

  //login
  static TextEditingController usernameController = TextEditingController();
  static TextEditingController passwordController = TextEditingController();

  //obscure textvalue passowrd
  static bool obscureText = true;
}
