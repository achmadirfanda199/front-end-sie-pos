// apps style
// ignore_for_file: deprecated_member_use

import 'package:front_end_sie_pos/apps_common_libs.dart';

class AppsStylesResource {
  static ThemeData themeData(bool isDarkTheme, BuildContext context) {
    final Color allbrownshade100 = Colors.brown.shade100;

    return ThemeData(
        appBarTheme: AppBarTheme(
          backgroundColor: isDarkTheme
              ? const Color.fromRGBO(31, 31, 31, 12)
              : const Color.fromRGBO(221, 136, 87, 87),
          titleTextStyle: Theme.of(context).textTheme.headline6!.copyWith(
                color: Colors.brown.shade200,
              ),
          elevation: 6.0,
        ),
        scaffoldBackgroundColor:
            isDarkTheme ? const Color.fromRGBO(18, 18, 18, 7) : Colors.white,
        cardColor: isDarkTheme
            ? const Color.fromRGBO(30, 30, 30, 12)
            : Colors.grey.shade200,
        primaryColor: isDarkTheme
            ? allbrownshade100
            : const Color.fromRGBO(214, 124, 65, 84),
        textTheme: TextTheme(
          headline1: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 27,
              fontWeight: FontWeight.w700),
          headline2: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 21,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.15),
          headline3: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 19,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.15),
          bodyText1: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.5),
          bodyText2: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              letterSpacing: 0.5),
          subtitle1: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w400,
              letterSpacing: 0.25),
          subtitle2: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 13,
              fontWeight: FontWeight.w400,
              letterSpacing: 0.25),
          button: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100: Colors.black,
              fontSize: 21,
              fontWeight: FontWeight.w600,
              letterSpacing: 0),
          caption: GoogleFonts.poppins(
              color: isDarkTheme ? allbrownshade100 : Colors.black,
              fontSize: 11,
              fontWeight: FontWeight.w400,
              letterSpacing: 0.4),
        ));
  }
}
