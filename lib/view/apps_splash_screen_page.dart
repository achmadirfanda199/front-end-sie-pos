// apps splash screen

// ignore_for_file: deprecated_member_use

import 'package:front_end_sie_pos/apps_common_libs.dart';

class AppsSplasscreen extends StatelessWidget {
  const AppsSplasscreen({super.key});
  Future<Widget> futureCall() async {
    return Future.delayed(
        const Duration(seconds: 3), (() => const AppsLoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    final caption = Theme.of(context).textTheme.caption;
    final scaffoldBackgroundColor = Theme.of(context).scaffoldBackgroundColor;
    return EasySplashScreen(
      logo: Image.asset(AppsIconResource.appsIcon),
      logoWidth: 80,
      backgroundColor: scaffoldBackgroundColor,
      showLoader: false,
      loadingText: Text('SiePOS | Empowering Your Business Transactions',
          style: caption!.copyWith(fontSize: 12, fontWeight: FontWeight.w700)),
      futureNavigator: futureCall(),
    );
  }
}
