// apps login page

// ignore_for_file: deprecated_member_use

import 'package:front_end_sie_pos/apps_common_libs.dart';

class AppsLoginPage extends StatefulWidget {
  const AppsLoginPage({super.key});

  @override
  State<AppsLoginPage> createState() => _AppsLoginPageState();
}

class _AppsLoginPageState extends State<AppsLoginPage> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size; 

    return AppsScaffold(
        withfloating: false,
        withappbar: false,
        withLeading: false,
        withAction: false,
        mainChildWidget: Stack(
          children: [
            if (mediaQuery.width < 500)
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Column(
                  children: [
                    ClipShadowPathWidget(
                        shadow: const Shadow(blurRadius: 16),
                        clipper: AppsCurveCliperWidget(),
                        childProperty: Image.asset(
                          AppsImageResource.loginwidth,
                          fit: BoxFit.cover,
                        )),
                    Container(
                        height: mediaQuery.height,
                        margin:
                            const EdgeInsets.only(left: 32, right: 32, top: 24),
                        child: SingleChildScrollView(
                            padding: const EdgeInsets.only(bottom: 500),
                            child: appsMainLoginWidget(context, setState))),
                  ],
                ),
              ),
            if (mediaQuery.width < 500)
              Positioned(
                  bottom: 24,
                  right: 0,
                  left: 0,
                  child: appsTailLoginWidget(context)),

            if (mediaQuery.width > 500)
            Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(AppsImageResource.loginheiht,),
                  fit: BoxFit.fill, 
                )
              ),
            ),
            
            if (mediaQuery.width>500)
            Positioned(
              top: (mediaQuery.height>700) ? mediaQuery.height/4 : (mediaQuery.height>500) ? mediaQuery.height/7 :  16,
              bottom: (mediaQuery.height > 700) ? mediaQuery.height / 4.3 : (mediaQuery.height>500) ? mediaQuery.height/7 : 36,
              left: 
              (mediaQuery.width>800) ? mediaQuery.width/4 :
              (mediaQuery.width>500)? mediaQuery.width/6 :
              
               mediaQuery.width/4,
              right: (mediaQuery.width > 800)
                    ? mediaQuery.width / 4
                    : (mediaQuery.width > 500)
                        ? mediaQuery.width / 6
                        : mediaQuery.width/4,
              child: Container(
                padding: const EdgeInsets.only(top: 8, left: 32, right: 32),
                height: mediaQuery.height,
                decoration: BoxDecoration(
                   color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.95), 
                  borderRadius: BorderRadius.circular(20), 
                  border: Border.all(color: Theme.of(context).primaryColor)
                ),
                child: SingleChildScrollView(
                  padding: const EdgeInsets.only(bottom: 80),
                  child: appsMainLoginWidget(context, setState),
                ),
              ),
            ),

            if(mediaQuery.width>500)
            Positioned(
              left: 0,
              right: 0,
              bottom: 5,
              child: appsTailLoginWidget(context)
            )

          ],
        ));
  }
}
