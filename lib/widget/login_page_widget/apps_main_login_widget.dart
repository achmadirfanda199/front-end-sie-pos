// apps formfield login widget

// ignore_for_file: deprecated_member_use

import 'package:front_end_sie_pos/apps_common_libs.dart';

Widget appsMainLoginWidget(
  context,
  setState,
) {
  return Form(
      key: AppsPublicVariableResource.formkey,
      child: Column(children: [
        Align(
            alignment: Alignment.center,
            child: Column(children: [
              Text('Selamat datang di SiePOS',
                  style: Theme.of(context).textTheme.headline1, textAlign: TextAlign.center,),
              Text('Silahkan masuk ke akun anda',
                  style: Theme.of(context).textTheme.headline3, textAlign: TextAlign.center,),
            ])),
        AppsPublicVariableResource.jarakheight32,
        AppsTextFieldWidget(
            controller: AppsPublicVariableResource.usernameController,
            jenisField: JenisField.universal,
            label: "Nama Pengguna",
            hintText: "namapengguna",
            namaField: "Nama Pengguna",
            maxLines: 1,
            keyboardType: TextInputType.text,
            suffixIcon: Icon(
              Icons.person,
              color: Theme.of(context).primaryColor,
            ),
            withValidaor: true,
            onChanged: (value) {},
            onsaved: (value) {
              AppsPublicVariableResource.username = value;
            }),
        AppsPublicVariableResource.jarakheight24,
        AppsTextFieldWidget(
            controller: AppsPublicVariableResource.passwordController,
            jenisField: JenisField.password,
            label: "Kata Sandi",
            hintText: "katasandi",
            namaField: "Kata Sandi",
            maxLines: 1,
            keyboardType: TextInputType.text,
            suffixIcon: IconButton(
              icon: (AppsPublicVariableResource.obscureText)
                  ? Icon(
                      Icons.visibility_off,
                      color: Theme.of(context).primaryColor,
                    )
                  : Icon(
                      Icons.visibility,
                      color: Theme.of(context).primaryColor,
                    ),
              onPressed: () {
                setState(() {
                  AppsPublicVariableResource.obscureText =
                      !AppsPublicVariableResource.obscureText;
                });
              },
            ),
            withValidaor: true,
            onChanged: (value) {},
            onsaved: (value) {
              AppsPublicVariableResource.password = value;
            }),
        AppsPublicVariableResource.jarakheight32,
        AppsButtonWidget(
            buttonType: ButtonType.normalButton,
            titleButton: "Masuk",
            backgroundColorButton: Theme.of(context).primaryColor,
            navigatorButton: () {
              appsLoginFunction();
            }),
        AppsPublicVariableResource.jarakheight24,
        Row(
          children: [
            Expanded(child: basicDivider(context)),
            AppsPublicVariableResource.jarakwidth8,
            Text(
              "atau daftarkan akun baru",
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(color: Colors.grey.shade300),
                  overflow: TextOverflow.ellipsis,
            ),
            AppsPublicVariableResource.jarakwidth8,
            Expanded(child: basicDivider(context)),
          ],
        ),
        AppsPublicVariableResource.jarakheight8,
        AppsButtonWidget(
            backgroundColorButton: Theme.of(context).scaffoldBackgroundColor,
            customTextStyleButton: Theme.of(context)
                .textTheme
                .button!
                .copyWith(color: Theme.of(context).primaryColor, fontSize: 20),
            titleButton: "Daftar",
            buttonType: ButtonType.normalButton,
            navigatorButton: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => const AppsRegisterPage()));
            })
      ]));
}
