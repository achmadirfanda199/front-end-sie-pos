// apps tail widget

// ignore_for_file: deprecated_member_use

import 'package:front_end_sie_pos/apps_common_libs.dart';

Widget appsTailLoginWidget(
  context,
) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Image.asset(
        AppsIconResource.appsIcon,
        width: 28,
        height: 28,
      ),
      const SizedBox(
        width: 5,
      ),
      Text('SiePOS | Empowering Your Business Transactions',
          style: Theme.of(context)
              .textTheme
              .caption!
              .copyWith(fontSize: 12, fontWeight: FontWeight.w700),
              overflow: TextOverflow.ellipsis,
              
              ),
    ],
  );
}
